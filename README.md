# Smashing the Barriers to OSS Contribution
## A Guide to Improving the Contributor Experience for Neurodivergent Individuals

### Project Goals:

*  This guide is intended for those involved in open source software to help address some common issues those that are neurodivergent face when contributing to OSS.
    *  These issues were identified by participants in 'Smashing the Barriers to OSS Contribution,' and by Rin Oliver during their workshop at Mozilla Festival 2019. Slides from this presentation are available for viewing at: https://www.slideshare.net/CKRinOliver/smashing-the-barriers-to-oss-contribution-187356432
    *  The issues listed here represent the lived experiences and opinions of those present during the workshop, along with Rin Oliver's own feedback and contributions during the workshop, which encompassed both neurodivergent and neurotypical attendees. 
        *  These are not intended to be representative (nor can they possibly be) of every individual neurodivergent person's lived experience. 
         
 * Please be kind, as this guide is a collaborative work in progress.

### What You'll Gain from Reading:

* A better understanding of how your organization can improve its overall contributor experience for those that are neurodivergent
* Understanding of accessibility issues that neurodivergent people face when contributing to OSS

### What is Neurodiversity?

* Neurodiversity is an umbrella term for the following: Autism, ADD/ADHD, Tourette’s, dyspraxia, dyslexia, dyscalculia, specific speech conditions, sensory processing conditions, and more.
* Note that semantically, an individual is **neurodivergent**, while 'neurodiversity' is the term used to describe the above. 

### Issues Relating to Lack of/Unclear Feedback

* Not confident that my contribution has value. Perfectionism.
* Being unsure if contribution is going to be evaluated and having to find out on top of this who to ask about that.
* Lack of feedback on one's contribution. 'I didn't know if I was doing well or not,' Positive feedback is important for continued contribution.
* Not clear what contribution is welcome, or even if any are welcome.
* Finding out where to report a bug/solution (if neurodivergent, having to expose this difficulty can be an issue)
* Finding out how to start contributing

### Obstacles Facing First Time Contributors

* "Good first issue," that is really just, "Can someone else," and no help when you ask.
* Hard to find good/"easy" first contributions
* No clear description of the help the project needed (this can be an even bigger challenge for detail-oriented people)
* Getting a grasp of the project, especially with large projects.
* Lack of resources if something goes wrong in setup

### Issues Currently Facing New OSS Contributors Both Neurodivergent and Neurotypical
#### These issues were identified by participants in my 'Smashing the Barriers to OSS Contribution,' workshop that took place on 10/27/2019 at Mozilla Festival:

* Open source is hard to get into. As the ‘hows’ are not very clear, and neurodivergent people may not understand the process that easily.
* Lack of documentation on how to get started or set up.
* No time or money to do free work and to learn new skills.
* No localised documentation.
* ‘Most of the way’ work gets rejected, so putting in effort doesn’t feel appreciated.
* Getting into arguments because the language is either too complex, or confusions don’t get cleared up.
* Having to register to a website–Which name to put there?
* Unclear social norms for asking about/learning about project

### Welcoming Open Source Contributions -- What to Consider:

* Don't use jargon
* Have a glossary
* Don't always assume people know what you mean
* Consider accessibility needs (screen readers, captioning, font readability, etc.)
* Use positive language
* Languages and cultures are diverse. Respect this!
* Have designated mentors that contributors or those considering contributing can go to with questions or concerns
* Have clear expectations of what you would like contributions to be comprised of
* Have neurodivergent mentors if possible on your project's team
* Keep in mind all neurodivergent people are different. If you've met one neurodivergent person, you have, in fact--Met only one neurodivergent person.
* Often we think of only hiring/including people with a good/"easy" community fit, but remember this can mean nondisruptive but different people

### Helpful Best Practices to Implement:

* Strive to give instructions one at a time
* Having one point of contact/a mentor/a Directly Responsible Individual (DRI) new contributors can go to with questions or issues
* Don't expect people to know all the rules, even if they're present online somewhere. Instead, help point people to docs welcomingly.
* Clear structure/schedule for community meetings
* Offering help submitting a contribution
* Socially facilitating/helping new contributors with getting started speaking in front of others (for those interested in doing so) --Finding 'your people' is key

### How Can We Better Structure the Open Source Software Contributor Experience?

* Clearer documentation
* Being aware of, anticipating, and designing your community to be accessibile. This can be done by using transcript services such as Otter.ai, closed captioning when giving a presentation using slides, and more
* Be up front with the current needs of your project and expected time commitments of new contributors
* Have an open and inclusive environment


### Proposed Solutions for Improving the Contributor Experience for Neurodivergent Individuals:

* Better pathways to contributing
* Consider all abilities (beginner, intermediate, expert)
* Not everyone comes from a Computer Science background. Make space for those from non-traditional backgrounds.
* Detailed contributing guidelines
* Require and enforce a community code of conduct for your OSS project
* Identify New Contributor's assigned Point of Contact for any questions/concerns
* Needed: More advocates and mentors
